package main

import (
	"fmt"

	"gitee.com/xihuanlanlande/ding-send/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
	}
}
