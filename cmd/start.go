package cmd

import (
	"gitee.com/xihuanlanlande/ding-send/apps/dingding/impl"
	"gitee.com/xihuanlanlande/ding-send/apps/http"
	"gitee.com/xihuanlanlande/ding-send/conf"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/spf13/cobra"
)

var (
	configPath string
)
var StartCmd = &cobra.Command{
	Use:   "start",
	Short: "启动 demo 后端API",
	Long:  "启动 demo 后端API",
	RunE: func(cmd *cobra.Command, args []string) error {
		zap.DevelopmentSetup()
		err := conf.LoadConfigFromToml(configPath)
		if err != nil {
			return err
		}
		r := gin.Default()
		if conf.C().Type == conf.TextFormat && conf.C().App == conf.DingApp {
			textobj := impl.NewDingText()
			handler := http.NewHandler(textobj)
			handler.Registry(r)
			r.Run()
		}

		return nil
	},
}

func init() {
	StartCmd.PersistentFlags().StringVarP(&configPath, "config", "f", "etc/demo.toml", "demo api 配置文件路径")
	RootCmd.AddCommand(StartCmd)
}
