package conf

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

type Ding struct {
	ACCESS_TOKEN string
	Secret       string
	Type         TypeFormat `toml:"type"`
}

type DingUser struct {
	Ding      `toml:"ding"`
	GroupUser map[string]GListU `toml:"Group"`
}

type GListU struct {
	UserList []string `toml:"userList"`
}

func NewDingUser() *DingUser {
	return &DingUser{
		Ding:      Ding{},
		GroupUser: map[string]GListU{},
	}
}

type ServiceApp struct {
	DingUser
	App AppType
}

func NewServiceApp() *ServiceApp {
	return &ServiceApp{
		DingUser: *NewDingUser(),
		App:      "",
	}
}

var config *ServiceApp

func LoadConfigFromToml(filePath string) error {
	config = NewServiceApp()
	_, err := toml.DecodeFile(filePath, config)
	if err != nil {
		return fmt.Errorf("load config from file error, path:%s, %s", filePath, err)
	}
	return nil
}

func C() *ServiceApp {
	return config
}
