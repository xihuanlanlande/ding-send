package conf_test

import (
	"fmt"
	"testing"

	"gitee.com/xihuanlanlande/ding-send/conf"
	"github.com/stretchr/testify/assert"
)

func TestLoadConfigFromToml(t *testing.T) {
	should := assert.New(t)
	// err := conf.LoadConfigFromToml("../etc/demo.toml")
	err := conf.LoadConfigFromToml("G:\\demo\\ding-send\\etc\\demo.toml")
	if should.NoError(err) {
		fmt.Println(*conf.C())
	}
}
