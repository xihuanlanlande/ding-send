package conf

type TypeFormat string

const (
	TextFormat = TypeFormat("text")
)

type AppType string

const (
	DingApp = AppType("ding")
)
