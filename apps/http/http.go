package http

import (
	"context"

	"gitee.com/xihuanlanlande/ding-send/apps"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	svc Msger
}

type Msger interface {
	Image(ctx context.Context, msgstr *apps.Image) error
}

func NewHandler(svc Msger) *Handler {
	return &Handler{
		svc: svc,
	}
}

func (h *Handler) Registry(r *gin.Engine) {
	r.POST("/push_image", h.pushImage)
}
