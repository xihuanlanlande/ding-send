package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"gitee.com/xihuanlanlande/ding-send/apps"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/http/response"
)

func (h *Handler) pushImage(c *gin.Context) {
	data, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		response.Failed(c.Writer, err)
	}
	fmt.Println(string(data))
	reqstr := apps.NewImage()
	json.Unmarshal(data, reqstr)
	err = h.svc.Image(c.Request.Context(), reqstr)
	if err != nil {
		response.Failed(c.Writer, err)
		return
	}
	response.Success(c.Writer, "sucess")
}
