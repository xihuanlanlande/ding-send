package impl

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitee.com/xihuanlanlande/ding-send/apps"
	"gitee.com/xihuanlanlande/ding-send/conf"
)

func (d *DingText) Image(ctx context.Context, msgstr *apps.Image) error {
	// msgstruct := NewMsgStruct()

	fmt.Println(*msgstr)
	Msg := NewDefaultTextMsg()
	var userList conf.GListU
	var has bool
	if userList, has = d.GListU[msgstr.EventData.Repository.Namespace]; !has {
		d.L.Debug("group is not has")
		return fmt.Errorf("group is not has")
	}

	Msg.At.AtMobiles = &userList.UserList
	uList := strings.Join(userList.UserList, "@")
	var imageList []string
	for _, imageName := range msgstr.EventData.Resources {
		imageList = append(imageList, imageName.ResourceURL)
	}
	imageListStr := strings.Join(imageList, "\n")
	// imageNotice="项目组:%s\n操作类型:%s\n镜像名称:%s"
	Msg.Text.Content = fmt.Sprintf(imageNotice, msgstr.EventData.Repository.Namespace, msgstr.Type, imageListStr, uList)
	d.ServiceMsg(ctx, Msg)
	return nil
}

//加签 url 生成
func (d *DingText) webhookUrl() string {
	//把timestamp+"\n"+密钥当做签名字符串，使用HmacSHA256算法计算签名
	timeStampNow := time.Now().UnixMilli()
	signStr := fmt.Sprintf("%d\n%s", timeStampNow, d.Secret)
	hash := hmac.New(sha256.New, []byte(d.Secret))
	hash.Write([]byte(signStr))
	sum := hash.Sum(nil)
	//进行Base64 encode，最后再把签名参数再进行urlEncode
	encode := base64.StdEncoding.EncodeToString(sum)
	urlEncode := url.QueryEscape(encode)
	return fmt.Sprintf("%s%s&timestamp=%d&sign=%s", webhookUrl, d.ACCESS_TOKEN, timeStampNow, urlEncode)
}

//生成url
func (d *DingText) RequsetUrl() string {
	return d.webhookUrl()
}

//生成文本内容
func (d *DingText) ServiceMsg(ctx context.Context, msg *TextMsg) error {
	msgbyte, err := json.Marshal(msg)
	fmt.Println(string(msgbyte))
	if err != nil {
		d.L.Error("jsonMarshal error")
		return fmt.Errorf("%s", err)
	}

	if err := d.SendMsg(ctx, msgbyte); err != nil {
		return err
	}

	return nil
}

//发送钉钉信息
func (d *DingText) SendMsg(ctx context.Context, msg []byte) error {
	client := &http.Client{}
	fmt.Println(d.webhookUrl())
	req, err := http.NewRequest("POST", d.webhookUrl(), bytes.NewReader(msg))
	if err != nil {
		d.L.Error("Send NewRequest error", err.Error())
		return fmt.Errorf("%s", err)
	}
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	resp, err := client.Do(req)
	if err != nil {
		d.L.Error("Send respone error", err.Error())
		return fmt.Errorf("%s", err)
	}
	defer resp.Body.Close()
	result, _ := io.ReadAll(resp.Body)
	fmt.Println(string(result))

	if resp.StatusCode == 200 {
		fmt.Println("发送成功")
		d.L.Debug("发送成功")
	} else {
		d.L.Warn(string(result))
	}
	return nil

}
