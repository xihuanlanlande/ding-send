package impl

import "gitee.com/xihuanlanlande/ding-send/apps/dingding"

type DingText struct {
	dingding.DingdingServiceImpl
}

func NewDingText() *DingText {
	return &DingText{
		DingdingServiceImpl: *dingding.NewDingdingServiceImpl(),
	}
}
