package impl_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/xihuanlanlande/ding-send/apps/dingding"
	"gitee.com/xihuanlanlande/ding-send/apps/dingding/impl"
	"gitee.com/xihuanlanlande/ding-send/conf"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/stretchr/testify/assert"
)

var dingText impl.DingText

func TestSend(t *testing.T) {
	should := assert.New(t)
	// msgstr := impl.NewDefaultTextMsg()
	// userList := []string{"17640348295","18842854114"}
	// msgstr.At = impl.At{
	// 	AtMobiles: &userList,
	// }
	// msgstr.Text = impl.TextContext{Content: "@18842854114\n要吃饭了"}

	// err := dingText.ServiceMsg(context.Background(), msgstr)
	err := dingText.MsgStruct(context.Background(), "test")
	if should.NoError(err) {
		fmt.Printf(dingText.RequsetUrl())
	}
}

func NewDefaultTextMsg() {
	panic("unimplemented")
}

func init() {
	zap.DevelopmentSetup()
	conf.LoadConfigFromToml("G:\\demo\\ding-send\\etc\\demo.toml")
	dingText = impl.DingText{
		DingdingServiceImpl: *dingding.NewDingdingServiceImpl(),
	}

	// dingText = impl.DingText{
	// 	DingdingServiceImpl: impl.DingdingServiceImpl{
	// 		L:            zap.L().Named("ding"),
	// 		ACCESS_TOKEN: "3afaee8dfb697fd62cae9db9d21be63ad918ebc176b74c980490b6febf6d5a4b",
	// 		Secret:       "SEC18de5d6768c263d7482dcfd5459049a89ea050d4ebf062c6ee9115212a19c186",
	// 	},
	// }
}
