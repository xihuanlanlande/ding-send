package impl

type msgtype string

const (
	Text msgtype = "text"
)

const (
	webhookUrl = "https://oapi.dingtalk.com/robot/send?access_token="
)

//text
type TextMsg struct {
	SendUser
	Text    TextContext `json:"text"`
	Msgtype msgtype     `json:"msgtype"`
}

type TextContext struct {
	Content string `json:"content"`
}

//markdown
type MarkdownMsg struct {
	SendUser
	markdown []map[string]string
	msgtype  msgtype
}

//发送用户
type SendUser struct {
	At At `json:"at"`
}
type At struct {
	AtMobiles *[]string `json:"atMobiles"`
	IsAtAll   bool      `json:"isAtAll"`
}

type MsgStruct struct {
	Group   string
	TextMsg *TextMsg
}

func NewMsgStruct() *MsgStruct {
	return &MsgStruct{
		Group:   "demo",
		TextMsg: NewDefaultTextMsg(),
	}
}

//初始化 text结构体
func NewDefaultTextMsg() *TextMsg {
	return &TextMsg{
		SendUser: SendUser{
			At: At{
				AtMobiles: &[]string{},
				IsAtAll:   false,
			},
		},
		Text:    TextContext{Content: "我就是我, @XXX 是不一样的烟火"},
		Msgtype: Text,
	}
}
