package dingding

import (
	

	"gitee.com/xihuanlanlande/ding-send/conf"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)



type DingdingServiceImpl struct {
	L            logger.Logger
	ACCESS_TOKEN string
	Secret       string
	GListU       map[string]conf.GListU
}

func NewDingdingServiceImpl() *DingdingServiceImpl {
	return &DingdingServiceImpl{
		L:            zap.L().Named("ding"),
		ACCESS_TOKEN: conf.C().ACCESS_TOKEN,
		Secret:       conf.C().Secret,
		GListU:       conf.C().GroupUser,
	}
}
